tool
class_name SnakeCase
extends Reference


func fix_path(path = null, ignorer = Ignorer.new()):
	path = path if path != null else "res://"
	traverse_path(path, self, "fix_file_name", ignorer)
	
	
static func reformat_name(name):
	var extension = name.get_extension() # Treat extension separately (no snake_casing, just to_lower())
	
	name = name.replace(extension, "")   # Continue with extension stripped (last dot remains intact)
	var new_name = name[0]               # Initialize with first character, which will only get lower cased
	
	for i in range(1, name.length()):    # Start reformat from the second character
		var c = name[i]
		new_name += c
		if is_upper_case(c) and new_name[-2] != ".":
			new_name = new_name.insert(new_name.length() - 1, "_")
	new_name = new_name.replace("__", "_")
	return new_name.to_lower() + (extension.to_lower() if extension.length() > 0 else "")


static func is_upper_case(letter):
	assert(letter.length() == 1)
	return letter.to_lower() != letter


static func traverse_path(path, target:Object, callback, ignorer := Ignorer.new()):
	var dir = Directory.new()
	if dir.open(path) == OK:
		dir.list_dir_begin(true, true)
		var file_name = dir.get_next()
		while file_name != "":
			if not ignorer.ignore(file_name):
				target.call(callback, dir, file_name)
				if dir.current_is_dir():
					traverse_path(dir.get_current_dir().plus_file(file_name), target, callback, ignorer)
			else: 
				print("Ignored: %s" % file_name)
			file_name = dir.get_next()
	else:
		print("An error occurred when trying to access the %s." % path)


static func fix_file_name(dir:Directory, file_name):
	var new_name = reformat_name(file_name)
	if (new_name != file_name):
		print("%s -> %s" % [file_name, new_name])
		rename_file(dir, file_name, new_name)


static func rename_file(dir:Directory, original_name, new_name):
	var result = dir.rename(original_name, new_name)

