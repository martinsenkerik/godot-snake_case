tool
extends ConfirmationDialog
class_name OptionsDialog

var preview


func is_ignoring_addons():
	return $"MainContainer/IgnoreContainer/Addons CheckBox".pressed


func is_ignoring_cs_scripts():
	return $"MainContainer/IgnoreContainer/C# scripts CheckBox".pressed


func is_ignoring_gd_scripts():
	return $"MainContainer/IgnoreContainer/GDScript CheckBox".pressed


func is_ignoring_scenes():
	return $"MainContainer/IgnoreContainer/Scenes CheckBox".pressed

func get_ignorer():
	return Ignorer.new(is_ignoring_addons(), is_ignoring_gd_scripts(), is_ignoring_scenes(), is_ignoring_cs_scripts())
	
func _on_ignore_changed(button_pressed):
	preview = ""
	SnakeCase.traverse_path("res://", self, "add_preview_file", get_ignorer())
	$MainContainer/PreviewContainer/Panel/Preview.text = preview


func add_preview_file(dir: Directory, file_name):
	print("Rename: " + file_name)
	var new_name = SnakeCase.reformat_name(file_name)
	if new_name != file_name:
		preview += dir.get_current_dir().plus_file(file_name) + " -> " + new_name + "\n"


func _on_about_to_show():
	_on_ignore_changed(null)


func _on_confirmed():
	SnakeCase.new().fix_path("res://", get_ignorer())
