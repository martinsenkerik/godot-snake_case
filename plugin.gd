tool
extends EditorPlugin

const MENU_ITEM_NAME = "Rename files to `snake_case`..."
const OPTIONS_DIALOG_UI := preload("res://addons/snake_case/options_dialog.tscn")

var dialog : OptionsDialog
var snake_case := SnakeCase.new()

func _enter_tree():
	add_tool_menu_item(MENU_ITEM_NAME, self, "open_options")
	dialog = OPTIONS_DIALOG_UI.instance() as OptionsDialog
	# Now we need to add the options dialog to the tree. As it is a dialog
	# and will appear above the main window anyway, we can include it anywhere.
	get_editor_interface().get_file_system_dock().add_child(dialog)
	dialog.connect("confirmed", self, "on_options_confirmed")

func _exit_tree():
	remove_tool_menu_item(MENU_ITEM_NAME)
	if dialog != null:
		get_editor_interface().get_file_system_dock().remove_child(dialog)
		dialog.queue_free()


func open_options(ignored):
	dialog.popup_centered()


func on_options_confirmed():
	var i := Ignorer.new(
			dialog.is_ignoring_addons(), 
			dialog.is_ignoring_gd_scripts(), 
			dialog.is_ignoring_scenes(), 
			dialog.is_ignoring_cs_scripts()
		)
	snake_case.fix_path(null, i)
	refresh_file_system_dock()


func refresh_file_system_dock():
	get_editor_interface().get_resource_filesystem().scan()

