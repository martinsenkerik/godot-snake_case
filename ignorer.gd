tool
extends Reference
class_name Ignorer

var addons
var gdscript
var scenes
var cs_scripts
var import


func _init(addons = true, gdscript = true, scenes = true, cs_scripts = true, import = true):
	self.addons = addons
	self.gdscript = gdscript
	self.scenes = scenes
	self.cs_scripts = cs_scripts
	self.import = import
	print(self)

func ignore(file_name: String):
	if file_name == null or file_name.length() < 1:
		return true

	if addons and ignore_addons(file_name):
		return true
	if gdscript and ignore_gdscript(file_name):
		return true
	if scenes and ignore_scene(file_name):
		return true
	if cs_scripts and ignore_cs_script(file_name):
		return true
	if import and ignore_import(file_name):
		return true
		
	return false


func ignore_addons(file_name:String):
	return file_name.to_lower() == "addons"


func ignore_gdscript(file_name:String):
	return file_name.to_lower().get_extension() == "gd"


func ignore_scene(file_name:String):
	return file_name.to_lower().get_extension() == "tscn"


func ignore_cs_script(file_name:String):
	return file_name.to_lower().get_extension() == "cs"


func ignore_import(file_name:String):
	return file_name.to_lower().get_extension() == "import"


func _to_string():
	return "Ignorer(addons=%s, gdscript=%s, scenes=%s, cs_scripts=%s, import=%s)" % [addons, gdscript, scenes, cs_scripts, import]
